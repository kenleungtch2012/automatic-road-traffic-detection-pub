import Knex from 'knex';
import dotenv from 'dotenv'
import fs from 'fs'
dotenv.config()

const knexConfig = require('../knexfile');
const knex = Knex(knexConfig[process.env.NODE_ENV ?? 'development'])

export class CarService {
    
    constructor(private knex: Knex) { }

    public async insertVideo(filename: string, district: string, road: string, date: string, time: string) {
        await knex('file').insert(
            { name: `${filename}`, district: `${district}`, road: `${road}`, date: `${date}`, time_interval: `${time}` },
        ).then(buff => { console.log('buff', buff) })
            .catch(err => { console.log('error:', err) })
    }

    public async getTxtOutput() {
        let fileRootPath = '/Users/kenleung/Documents/Tecky-Academy/_project/automatic-road-traffic-detection/AI_detection/txt_output/'
        let carAmountObj = {}
        await fs.promises.readdir(fileRootPath)

            .then(async filename => {
                console.log('filename', filename);
                await fs.promises.readFile(fileRootPath + filename)

                    .then(async (carAmount) => {
                        console.log('service content', carAmount.toString())

                        const filenameTrim = filename[0].replace("_output.txt", '')
                        const newFilename = '%' + filenameTrim + '%'
                        console.log('filenameTrim: ', newFilename)
                        const videoRecord = await this.getVideoRecord(newFilename)
                        console.log('videoRecord: ', videoRecord)

                        const videoDate = videoRecord[0]["date"]
                        const videoRoadName = videoRecord[0]['road']
                        const videoTimeInterval = videoRecord[0]['time_interval'].replace(/\:*\s*/gm, '')


                        const carAmountStr = carAmount.toString()
                        const carAmountInt = parseInt(carAmountStr)
                        console.log(videoDate, videoRoadName, videoTimeInterval, carAmountInt)

                        await this.addCarAmount2Report(videoDate, videoRoadName, videoTimeInterval, carAmountInt)

                        carAmountObj = { "count": carAmountStr }
                        console.log('service contentStr', carAmountObj, ', type:', typeof (carAmountObj))
                        return carAmountObj
                    })
            })
        return carAmountObj
    }

    public async getVideoRecord(videoName: string) {
        return await this.knex
            .from("file")
            .select("*")
            .where("name", "like", videoName)
    }

    public async getCar(roadName: number, date: Date) {
        return await this.knex
            .from("car_report")
            .select("*")
            .where('road_id', '=', roadName)
            .where('date', '=', date)
    }

    public async addCarReport(report: object) {
        await this.knex.insert({
            "date": Date,
            "road": String,
            "district": String,
            "0900-1000": Number,
            "1000-1100": Number,
            "1100-1200": Number,
            "1200-1300": Number,
            "1300-1400": Number,
            "1400-1500": Number,
            "1500-1600": Number,
            "1600-1700": Number,
            "1700-1800": Number,

        }).into("car_report").returning("id")
    }

    public async addCarAmount2Report(date: string, roadName: string, timeRange: string, carAmount: number) {

        const roadId = await this.knex
            .select("id")
            .from("road")
            .where("road.name", "=", roadName)
        console.log(`got road_id = ${JSON.stringify(roadId[0])}`)

        const carReportObjStr = `{
            "date": "${date}",
            "road_id": ${roadId[0]['id']},
            "${timeRange}": ${carAmount}
        }`
        console.log(carReportObjStr)
        const carReportObjJson = JSON.parse(carReportObjStr)
        console.log(carReportObjJson)
        await this.knex
            .insert(carReportObjJson)
            .into("car_report")
            .returning("id")
    }

}