window.onload = function () {

  var chart = new CanvasJS.Chart("car-chart", {
    theme: "light1", // "light2", "dark1", "dark2"
    animationEnabled: false, // change to true		
    title:{
      text: "Vehicle Over Time"
    },
    data: [
    {
      // Change type to "bar", "area", "spline", "pie",etc.
      type: "column",
      dataPoints: [
        { label: "0900-1000",  y: 523  },
        { label: "1000-1100", y: 500  },
        { label: "1100-1200", y: 443  },
        { label: "1200-1300", y: 400  },
        { label: "1300-1400", y: 332  },
        { label: "1400-1500", y: 298  },
        { label: "1500-1600", y: 323  },
        { label: "1600-1700", y: 398  },
        { label: "1700-1800", y: 437  },
      ]
    }
    ]
  });
  chart.render();
  
  }
  