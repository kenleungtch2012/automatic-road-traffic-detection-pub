async function loadLineChart() {
    document.querySelector('#showSelect')
    .addEventListener('submit', async(event) => {
    // event.preventDefault();
    // })
    let res = await fetch ('/getCar', {method:"GET"});
    const json = await res.json();
    console.log('hi')
    console.log(json)
    const result = [json['0900-1000'], json['1000-1100'], json['1100-1200'], json['1200-1300'], json['1300-1400'], json['1400-1500'], json['1500-1600'], json['1600-1700'], json['1700-1800']]

    const chart = new CanvasJS.Chart("car-chart", {
        theme: "light1", // "light2", "dark1", "dark2"
        animationEnabled: false, // change to true		
        title:{
            text: "Vehicle Over Time"
        },
        data: {
                type: "column",
                labels: ['0900-1000', '1000-1100', '1100-1200', '1200-1300', '1300-1400', '1400-1500', '1500-1600', '1600-1700', '1700-1800'],
                datasets: [
                    {
                    type: 'line',
                        data: result,
                    },
                ]
            },
    });
    chart.render();
})
}

window.onload = loadLineChart()
