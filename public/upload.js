document.getElementById("upload-button").addEventListener('click', async (event) => {
    // event.preventDefault()

    document.getElementById("upload-button").innerHTML = 'Uploading ...'
    console.log('start to fetch...')
    await fetch('/upload')
        .then(() => { document.getElementById("upload-button").innerHTML = 'Upload Complete!' })
})

document.querySelector('#detect-button').addEventListener('click', async (event) => {
    event.preventDefault()

    document.querySelector('#detect-container').innerHTML = `<input type="button" class="btn btn-primary" value="Detecting ..." id="detect-button"></input>`
    document.querySelector("#detect-status").innerHTML = `<i class="fas fa-cog fa-spin"></i>`

    let result = false

    await fetch('http://localhost:8888/detect-car')
        .then(() => {
            result = true
            return result
        })
    console.log('result:', result)
    if (result) {
        let res = await fetch('/getTxtOutput', { method: "GET" });
        const content = await res.json();
        console.log('content:', content['count'])
        document.querySelector('#detect-container').innerHTML = `<input type="button" class="btn btn-primary" value="Detection Complete!" id="detect-button"></input>`
        document.querySelector("#detect-status").innerHTML = `Got ${content['count']} vehicle(s).`
    }

})