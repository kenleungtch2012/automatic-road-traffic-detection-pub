import express from 'express';
import { CarService } from '../services/CarService'

export class CarController {
    public constructor(private service: CarService) { }


    public getCar = async (req: express.Request, res: express.Response) => {
        console.log(req.body)
        // const reports = await this.service.getCar(req.body.district_id, req.body.road_id, req.body.date)
        const reports = await this.service.getCar(req.body.selectDetail.road_id, req.body.selectDetail.date)
        res.json(reports)
    };

    public getTxtOutput = async (req: express.Request, res: express.Response) => {
        const result = await this.service.getTxtOutput()
        console.log('controller result:', result)
        res.json(result)
    }

    public insertVideo = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        console.log(req.body)
        console.log('upload completed!')
        console.log(__dirname)
        console.log(req.body.district_id)
        console.log(req.body.road_id)
        console.log(req.body.date)
        console.log(req.file.filename)
        console.log(req.body.time_interval)

        const filename = req.file.filename;
        const district = req.body.district_id;
        const road = req.body.road_id;
        const date = req.body.date;
        const time = req.body.time_interval;
        await this.service.insertVideo(filename, district, road, date, time)
    }

}