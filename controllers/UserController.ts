import express from 'express';
import Knex from 'knex';
import { hashPassword, checkPassword } from '../hash'
const knexConfig = require('../knexfile');
const knex = Knex(knexConfig[process.env.NODE_ENV ?? 'development'])


export class UserController {

    public constructor() { }

    public register = async (req: express.Request, res: express.Response) => {
        console.log(req.body)
        const users = await knex.from('user')
            .select('*');
        console.log('userList= ',users)
        const lowercaseName1 = (req.body.username).toLowerCase();
        const lowercaseEmail1 = (req.body.email).toLowerCase();

        let checkUser = false

        for (const user of users) {
            if (user.username == lowercaseName1 || user.email == lowercaseEmail1) {
                console.log('user=',user)
                checkUser = true
                console.log('checkUser inside for-loop = ',checkUser)
            }
        }
        
        console.log('checkUser outside for-loop = ',checkUser)
        if (checkUser) {
            console.log(`Username: ${lowercaseName1} OR Email:${lowercaseEmail1} is already used!`);
            res.send(`<script>alert('Username: ${lowercaseName1} OR Email:${lowercaseEmail1} is already used!'); window.location.href = "/register.html"; </script>`);
        } else {
            console.log(`Username: ${lowercaseName1} OR Email:${lowercaseEmail1} is fine`);
            const myName = req.body.username;
            const lowercaseName = myName.toLowerCase();
            const myPassword = req.body.password;
            const registerHashPassword = await hashPassword(myPassword)
            const myEmail = req.body.email;
            console.log(myName, myPassword, myEmail);
            console.log('signed up user' + 'Your username:' + req.body.username + 'Your email' + req.body.email);
            console.log('Start inserting...');

            await knex('user').insert(
                { username: `${lowercaseName}`, password: `${registerHashPassword}`, email: `${req.body.email}` },
            ).then(buff => { console.log('insert completed!') }).then(() => {
                req.session['user'] = lowercaseName;
                console.log(`req.session['user'] = ${lowercaseName}`);
                res.send(`<script>alert("welcome!! ${lowercaseName}  You register successfully!!! You can login now!"); window.location.href = "/login.html"; </script>`);
            })
                .catch(err => { console.log('Failed to insert!', err) })
        }
    }

    public login = async (req: express.Request, res: express.Response) => {
        const users = await knex.from('user')
            .select('*');
        const lowercaseName = (req.body.username).toLowerCase();
        for (const user of users) {
            const matchPassword = await checkPassword(req.body.password, user.password)
            if (user.username == lowercaseName && matchPassword) {
                console.log(`database username:${user.name} password:${req.body.password} | ${user.password}`);
                if (req.session != null) {
                    req.session['user'] = lowercaseName;
                    console.log(`req.session['user'] = ${req.session['user']}`);
                    console.log('session not null. Welcome back.');
                    res.redirect('/index.html');
                }
                break;
            }
        }
        console.log(req.session['user'])
        return;
    }
}