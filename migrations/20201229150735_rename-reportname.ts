import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable('carreport')) {
        async () => {
            await knex.schema.renameTable('carreport', 'car_report')
        }
    }
    if (await knex.schema.hasTable('humanreport')) {
        async () => {
            await knex.schema.renameTable('humanreport', 'human_report')
        }
    }
}


export async function down(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable('car_report')) {
        async () => {
            await knex.schema.renameTable('car_report', 'carreport')
        }
    }
    if (await knex.schema.hasTable('human_report')) {
        async () => {
            await knex.schema.renameTable('human_report', 'humanreport')
        }
    }
}

