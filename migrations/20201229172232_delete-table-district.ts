import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.dropTable('car_report')
    await knex.schema.dropTable('human_report')
    await knex.schema.dropTable('road')
    await knex.schema.dropTable('district')
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.createTable('district', (table) => {
        table.increments()
        table.string('name').notNullable();
    })

    await knex.schema.createTable('road', (table) => {
        table.increments()
        table.string('name').notNullable()
        table.integer('district_id').unsigned()
        table.foreign('district_id').references('district.id')
    })

    await knex.schema.createTable('car_report', (table) =>{
        table.increments()
        table.date('date').notNullable
        table.integer('road_id').unsigned()
        table.foreign('road_id').references('road.id')
        table.integer("0900-1000")
        table.integer("1001-1100")
        table.integer("1101-1200")
        table.integer("1201-1300")
        table.integer("1301-1400")
        table.integer("1401-1500")
        table.integer("1501-1600")
        table.integer("1601-1700")
        table.integer("1701-1800")
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
    });

    await knex.schema.createTable('human_report', (table) =>{
        table.increments()
        table.date('date').notNullable()
        table.integer("0900-1000")
        table.integer("1001-1100")
        table.integer("1101-1200")
        table.integer("1201-1300")
        table.integer("1301-1400")
        table.integer("1401-1500")
        table.integer("1501-1600")
        table.integer("1601-1700")
        table.integer("1701-1800")
        table.integer('road_id').unsigned()
        table.foreign('road_id').references('road.id')
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
    })
}

