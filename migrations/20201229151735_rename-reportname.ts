import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.hasTable('carreport').then(async () => {
        await knex.schema.renameTable('carreport', 'car_report')
    })
    await knex.schema.hasTable('humanreport').then(async () => {
        await knex.schema.renameTable('humanreport', 'human_report')
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.hasTable('car_report').then(async () => {
        await knex.schema.renameTable('car_report', 'carreport')
    })
    await knex.schema.hasTable('human_report').then(async () => {
        await knex.schema.renameTable('human_report', 'humanreport')
    })
}

