import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.table('car_report', function (table) {
        table.timestamp('created_at').defaultTo(knex.fn.now());    })
    await knex.schema.table('human_report', function (table) {
        table.timestamp('updated_at').defaultTo(knex.fn.now());    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.table('car_report', function (table) {
        table.dropTimestamps();
    })
    await knex.schema.table('human_report', function (table) {
        table.dropTimestamps();
    })
}

