import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('file', (table) => {
        table.increments()
        table.string('name').notNullable();
        table.string('district').notNullable();
        table.string('road').notNullable();
        table.string('date').notNullable();
        table.string('time_interval').notNullable();
        table.string('status');
        table.timestamps(false, true);
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable('file')
}

