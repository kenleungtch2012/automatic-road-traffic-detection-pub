import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable('car_report')) {
        await knex.schema.alterTable('car_report',(table)=>{
            table.renameColumn('1001-1100', '1000-1100')
            table.renameColumn('1101-1200', '1100-1200')
            table.renameColumn('1201-1300', '1200-1300')
            table.renameColumn('1301-1400', '1300-1400')
            table.renameColumn('1401-1500', '1400-1500')
            table.renameColumn('1501-1600', '1500-1600')
            table.renameColumn('1601-1700', '1600-1700')
            table.renameColumn('1701-1800', '1700-1800')
        })
    };
    if (await knex.schema.hasTable('human_report')) {
        await knex.schema.alterTable('human_report',(table)=>{
            table.renameColumn('1001-1100', '1000-1100')
            table.renameColumn('1101-1200', '1100-1200')
            table.renameColumn('1201-1300', '1200-1300')
            table.renameColumn('1301-1400', '1300-1400')
            table.renameColumn('1401-1500', '1400-1500')
            table.renameColumn('1501-1600', '1500-1600')
            table.renameColumn('1601-1700', '1600-1700')
            table.renameColumn('1701-1800', '1700-1800')
        })
    };
}


export async function down(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable('car_report')) {
        await knex.schema.alterTable('car_report',(table)=>{
            table.renameColumn('1000-1100', '1001-1100')
            table.renameColumn('1100-1200', '1101-1200')
            table.renameColumn('1200-1300', '1201-1300')
            table.renameColumn('1300-1400', '1301-1400')
            table.renameColumn('1400-1500', '1401-1500')
            table.renameColumn('1500-1600', '1501-1600')
            table.renameColumn('1600-1700', '1601-1700')
            table.renameColumn('1700-1800', '1701-1800')
        })
    };
    if (await knex.schema.hasTable('human_report')) {
        await knex.schema.alterTable('human_report',(table)=>{
            table.renameColumn('1000-1100', '1001-1100')
            table.renameColumn('1100-1200', '1101-1200')
            table.renameColumn('1200-1300', '1201-1300')
            table.renameColumn('1300-1400', '1301-1400')
            table.renameColumn('1400-1500', '1401-1500')
            table.renameColumn('1500-1600', '1501-1600')
            table.renameColumn('1600-1700', '1601-1700')
            table.renameColumn('1700-1800', '1701-1800')
        })
    };

}

