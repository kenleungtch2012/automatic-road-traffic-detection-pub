import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.table('car_report', function (table) {
        table.dropColumn('created_at');
    })
    await knex.schema.table('human_report', function (table) {
        table.dropColumn('updated_at');
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.table('car_report', function (table) {
        table.timestamp('created_at').defaultTo(knex.fn.now());
    })
    await knex.schema.table('human_report', function (table) {
        table.timestamp('updated_at').defaultTo(knex.fn.now());
    })
}

