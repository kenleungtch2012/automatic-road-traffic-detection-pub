import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable('car_report')) {
        knex.schema.renameTable('1001-1100', '1000-1100')
        knex.schema.renameTable('1101-1200', '1100-1200')
        knex.schema.renameTable('1201-1300', '1200-1300')
        knex.schema.renameTable('1301-1400', '1300-1400')
        knex.schema.renameTable('1401-1500', '1400-1500')
        knex.schema.renameTable('1501-1600', '1500-1600')
        knex.schema.renameTable('1601-1700', '1600-1700')
        knex.schema.renameTable('1701-1800', '1700-1800')
    };
    if (await knex.schema.hasTable('human_report')) {
        knex.schema.renameTable('1001-1100', '1000-1100')
        knex.schema.renameTable('1101-1200', '1100-1200')
        knex.schema.renameTable('1201-1300', '1200-1300')
        knex.schema.renameTable('1301-1400', '1300-1400')
        knex.schema.renameTable('1401-1500', '1400-1500')
        knex.schema.renameTable('1501-1600', '1500-1600')
        knex.schema.renameTable('1601-1700', '1600-1700')
        knex.schema.renameTable('1701-1800', '1700-1800')
    };
}


export async function down(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable('car_report')) {
        knex.schema.renameTable('1000-1100', '1001-1100')
        knex.schema.renameTable('1100-1200', '1101-1200')
        knex.schema.renameTable('1200-1300', '1201-1300')
        knex.schema.renameTable('1300-1400', '1301-1400')
        knex.schema.renameTable('1400-1500', '1401-1500')
        knex.schema.renameTable('1500-1600', '1501-1600')
        knex.schema.renameTable('1600-1700', '1601-1700')
        knex.schema.renameTable('1700-1800', '1701-1800')
    };
    if (await knex.schema.hasTable('human_report')) {
        knex.schema.renameTable('1000-1100', '1001-1100')
        knex.schema.renameTable('1100-1200', '1101-1200')
        knex.schema.renameTable('1200-1300', '1201-1300')
        knex.schema.renameTable('1300-1400', '1301-1400')
        knex.schema.renameTable('1400-1500', '1401-1500')
        knex.schema.renameTable('1500-1600', '1501-1600')
        knex.schema.renameTable('1600-1700', '1601-1700')
        knex.schema.renameTable('1700-1800', '1701-1800')
    };
}

