
import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('report', (table) =>{
        table.dropColumn('car')
        table.dropColumn('people')
        table.dropColumn('time')
        table.integer("0900-1000")
        table.integer("1001-1100")
        table.integer("1101-1200")
        table.integer("1201-1300")
        table.integer("1301-1400")
        table.integer("1401-1500")
        table.integer("1501-1600")
        table.integer("1601-1700")
        table.integer("1701-1800")
    });

    await knex.schema.createTable('humanreport', (table) =>{
        table.increments()
        table.date('date').notNullable()
        table.integer("0900-1000")
        table.integer("1001-1100")
        table.integer("1101-1200")
        table.integer("1201-1300")
        table.integer("1301-1400")
        table.integer("1401-1500")
        table.integer("1501-1600")
        table.integer("1601-1700")
        table.integer("1701-1800")
        table.integer('road_id').unsigned()
        table.foreign('road_id').references('road.id')
    })

    await knex.schema.renameTable('report', 'carreport')
    
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.createTable('report', (table) =>{
        table.integer('car').unsigned()
        table.integer('people').unsigned()
        table.time('time').notNullable
    });

    await knex.schema.alterTable('report', (table) =>{
        table.dropColumn("0900-1000")
        table.dropColumn("1001-1100")
        table.dropColumn("1101-1200")
        table.dropColumn("1201-1300")
        table.dropColumn("1301-1400")
        table.dropColumn("1401-1500")
        table.dropColumn("1501-1600")
        table.dropColumn("1601-1700")
        table.dropColumn("1701-1800")
    });

    await knex.schema.dropTable('humanreport');

    await knex.schema.renameTable('carreport', 'report')
}
