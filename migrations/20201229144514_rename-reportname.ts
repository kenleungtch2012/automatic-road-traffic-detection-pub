import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable('carreport')) {
        knex.schema.renameTable('carreport', 'car_report')
    };
    if (await knex.schema.hasTable('humanreport')) {
        knex.schema.renameTable('humanreport', 'human_report')
    };
};


export async function down(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable('car_report')) {
        knex.schema.renameTable('car_report', 'carreport')
    };
    if (await knex.schema.hasTable('human_report')) {
        knex.schema.renameTable('human_report', 'humanreport')
    };
}

