import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('district', (table) => {
        table.increments()
        table.string('name').notNullable();
    })

    await knex.schema.createTable('road', (table) => {
        table.increments()
        table.string('name').notNullable()
        table.integer('district_id').unsigned()
        table.foreign('district_id').references('district.id')
    })

    await knex.schema.createTable('report', (table) =>{
        table.increments()
        table.date('date').notNullable
        table.integer('car').unsigned()
        table.integer('people').unsigned()
        table.time('time').notNullable
        table.integer('road_id').unsigned()
        table.foreign('road_id').references('road.id')
    })

}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable('district')
    await knex.schema.dropTable('road')
    await knex.schema.dropTable('report')
}

