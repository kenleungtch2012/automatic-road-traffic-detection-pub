import tornado.ioloop
import tornado.web
from AI_detection.AI import vehicle_detection_video

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("Welcome to Tornado Server.")

class detectCar(tornado.web.RequestHandler):

    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')

    async def get(self):
        vehicle_detection_video.main()

    def options(self):
        self.set_status(204)
        self.finish()    

def make_app():
    return tornado.web.Application([
        (r"/",MainHandler),
        (r"/detect-car",detectCar)
    ])

if __name__ == "__main__":
    app = make_app()
    app.listen(8888)
    print('listening to http://localhost:8888')
    tornado.ioloop.IOLoop.current().start()