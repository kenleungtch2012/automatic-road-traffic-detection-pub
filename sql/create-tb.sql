CREATE TABLE "district"(
    id SERIAL primary key,
    name VARCHAR(255) not null,
)

CREATE TABLE road(
    id Serial primary key,
    name VARCHAR(255) not null,
    district_id integer REFERENCES district(id)
)

CREATE TABLE report(
    id Serial primary key,
    car number,
    people number,
    date date,
    time time
)