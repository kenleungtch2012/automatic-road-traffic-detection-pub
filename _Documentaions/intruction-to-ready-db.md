
0. (before run seed next time, drop the database then go through the steps below to create a clean database)

1. create database "project2"

2. input "yarn" in terminal
<!-- to install packages -->

3. yarn init(if haven't do it at first, just like npm init)

4. create .env file and input like this (do this if you don't have it)
[
    DB_HOST=localhost
    DB_USERNAME=xxxx
    DB_PASSWORD=xxxx
    DB_NAME=project2
]

5. run "yarn knex migrate:latest"

6. run "yarn knex seed:run"