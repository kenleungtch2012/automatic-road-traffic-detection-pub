# 1. deactivate current environment
> conda deactivate

# 2. create a new environment

## create a new environment
format = conda create -n <env_name>
> conda create -n pj2

## create a new environment with specific "newest" python version
format = conda create -n <env_name> python=x.x
> conda create -n pj2py38 python=3.8
(it will get the newest version of 3.8 -> 3.8.x)

## create a new environment with specific python version
format = conda create -n <env_name> python=x.x.x
> conda create -n pj2py38 python=3.8.5
(it will get the python 3.8.5)

# 3. to list created environments
> conda env list

# 4. to activate an environment
format = conda activate <env>
> conda activate pj2py38
(then (pj2py38) will appear at the beginning)

# 5. to install a package in current active environment
format = conda install <package>
> conda install my-package-name

# 6. install "opencv" from a channel "conda-forge":
## run python and ML need to install openCV, you may run on other conda environment
format = conda install -c "conda-forge" package
> conda install -c conda-forge opencv

# 7. after installed the env & packages, run this command to run the Tornado server:
> python server.py