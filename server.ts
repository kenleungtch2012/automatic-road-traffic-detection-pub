import express from 'express';
import { Request, Response } from 'express';
import path from 'path';
import bodyParser from 'body-parser';
import multer from 'multer';
import expressSession from 'express-session';
import { CarController } from './controllers/CarController'
import { CarService } from './services/CarService'
import Knex from 'knex';
import dotenv from 'dotenv'
import { UserController } from './controllers/UserController';
dotenv.config()

const knexConfig = require('./knexfile');
const knex = Knex(knexConfig[process.env.NODE_ENV ?? 'development'])

const app = express();

const sessionSecret = process.env.EXPRESS_SESSION_SECRET
let sessionMiddleware = expressSession({
    secret: `${sessionSecret}`,
    resave: true,
    saveUninitialized: true
});
app.use(sessionMiddleware)

let storage = multer.diskStorage({
    destination: function (req, file, cb) {
        console.log(req.body)
        console.log(file)
        cb(null, `${__dirname}/public/uploads/video_source`);
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    }
})
const upload = multer({ storage })

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static('public'))

const carService = new CarService(knex);
const carController = new CarController(carService);
const userController = new UserController();

app.get('/', (req: Request, res: Response) => {
    console.log('at /', req.session['user'])
    const relativePath = path.resolve(__dirname, 'public')
    if (req.session['user'] == undefined) {
        res.sendFile(path.join(relativePath, 'login.html'));
    } else {
        res.sendFile(path.join(relativePath, 'index.html'));
    }
})

app.get('/logout', (req: Request, res: Response) => {
    delete req.session['user'];
    console.log('at /logout', req.session['user'])
    const relativePath = path.resolve(__dirname, 'public')
    res.sendFile(path.join(relativePath, 'login.html'));
})

app.get('/getCar', carController.getCar);
app.post('/getCar', carController.getCar);
app.post('/upload', upload.single('video'), carController.insertVideo)
app.get('/getTxtOutput', carController.getTxtOutput);
app.post('/register', userController.register)
app.post('/login', userController.login)

app.use((req, res) => {
    res.sendFile(path.resolve('./public/404.html'));
});

const PORT = 8080;
app.listen(PORT, () => {
    console.log(`listening to http://localhost:${PORT}`)
    console.log(`listening to http://kenleung2020.site`)

})