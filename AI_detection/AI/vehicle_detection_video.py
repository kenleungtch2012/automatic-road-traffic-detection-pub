import cv2
import numpy as np
import os


def main():

    #video_source_dir = '/full-path-of-the-project-root/public/uploads/video_source/'

    #video_output_dir = '/full-path-of-the-project-root/AI_detection/video_output/'

    #txt_output_dir = '/full-path-of-the-project-root/AI_detection/txt_output/'
    
    file_arr=os.listdir(video_source_dir)

    for file in file_arr:

        video_source_path = video_source_dir + file

        video_output_file = file.rstrip('.mp4') + '_output.mp4'
        video_output_path = video_output_dir + video_output_file

        txt_output_file = file.rstrip('.mp4') + '_output.txt'
        txt_output_path = txt_output_dir + txt_output_file


        # print(video_source_path)
        # print(video_output_path)

        min_contour_width=80  #40
        min_contour_height=80  #40
        offset=6       #10
        line_height=550 #550


        matches =[]
        cars=0
        def get_centroid(x, y, w, h):
            x1 = int(w / 2)
            y1 = int(h / 2)

            cx = x + x1
            cy = y + y1
            return cx,cy
                
        #cap = cv2.VideoCapture(0)
        cap = cv2.VideoCapture(video_source_path)
        size = (int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)), int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)))
        out = cv2.VideoWriter(video_output_path,cv2.VideoWriter_fourcc(*'mp4v'), 20, size)

        if cap.isOpened():
            ret,frame1 = cap.read()
        else:
            ret = False
        ret,frame1 = cap.read()
        ret,frame2 = cap.read()
            
        while ret:
            d = cv2.absdiff(frame1,frame2)
            grey = cv2.cvtColor(d,cv2.COLOR_BGR2GRAY)
            blur = cv2.GaussianBlur(grey,(1,1),0)
            ret , th = cv2.threshold(blur,20,255,cv2.THRESH_BINARY)
            dilated = cv2.dilate(th,np.ones((3,3)))
            kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (2, 2))

            closing = cv2.morphologyEx(dilated, cv2.MORPH_CLOSE, kernel) 
            contours,h = cv2.findContours(closing,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
            cv2.line(frame1, (50, line_height), (1200, line_height), (0,255,0), 2)
            for(i,c) in enumerate(contours):
                (x,y,w,h) = cv2.boundingRect(c)
                contour_valid = (w >= min_contour_width) and (
                    h >= min_contour_height)
                if not contour_valid:
                    continue
                cv2.rectangle(frame1,(x,y-10),(x+w+10,y+h+10),(255,0,0),2)
                
                centroid = get_centroid(x, y, w, h)
                matches.append(centroid)
                cv2.circle(frame1,centroid, 5, (0,255,0), -1)
                cx,cy= get_centroid(x, y, w, h)
                for (x,y) in matches:
                    if y<(line_height+offset) and y>(line_height-offset):
                        cars=cars+1
                        matches.remove((x,y))
                        
            cv2.putText(frame1, "Total Cars Detected: " + str(cars), (10, 90), cv2.FONT_HERSHEY_SIMPLEX, 1,
                            (0, 170, 0), 2)


            cv2.imshow("Difference" , dilated)
            # cv2.imshow("Original" , cv2.blur(frame1,(3,3)))

            if cv2.waitKey(1) == 27:
                break
            out.write(cv2.blur(frame1,(3,3)))
            frame1 = frame2
            ret , frame2 = cap.read()


        out.release()
        cap.release()
        # cv2.destroyAllWindows()


        txt_file = open(txt_output_path,"w") 
        txt_file.write(str(cars))
        txt_file.close() 

main()
